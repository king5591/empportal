import { EmpComponent } from "./emp/emp.component";
import { AdminComponent } from "./admin/admin.component";
import { LoginComponent } from "./login/login.component";
import { NgModule, Component } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "admin", component: AdminComponent },
  { path: "emp", component: EmpComponent },
  { path: "emp/:portal", component: EmpComponent },
  { path: "emp/:portal/:id", component: EmpComponent },
  { path: "admin/:empDetail", component: AdminComponent },
  { path: "**", redirectTo: "login", pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
