import { HttpClientModule } from "@angular/common/http";
import { NetService } from "./net.service";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { AdminComponent } from "./admin/admin.component";
import { EmpComponent } from './emp/emp.component';

@NgModule({
  declarations: [AppComponent, LoginComponent, AdminComponent, EmpComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [NetService],
  bootstrap: [AppComponent],
})
export class AppModule {}
