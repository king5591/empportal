import { NetService } from "./../net.service";
import { Component, OnInit, Input } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  ValidatorFn,
  AbstractControl,
} from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  simpleForm: FormGroup = null;
  eid: string = "";
  pass: string = "";
  data;
  url: string = "http://localhost:2410/empapp/emps";
  status = false;
  login;

  constructor(
    private netService: NetService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.netService.getData(this.url).subscribe((resp) => {
      this.data = resp;
      this.data = this.data.data;
      // console.log(this.data);
    });
    this.createLoginForm();
  }

  createLoginForm() {
    this.simpleForm = new FormGroup({
      id: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [Validators.required]),
    });
  }

  get id() {
    return this.simpleForm.get("id");
  }
  get password() {
    return this.simpleForm.get("password");
  }

  onSubmit() {
    this.eid = this.simpleForm.get("id").value;
    this.pass = this.simpleForm.get("password").value;
    // console.log(this.eid);
    this.login = this.data.find(
      (d1) => d1.email == this.eid && d1.password == this.pass
    );
    // console.log(this.status);
    if (this.login) {
      if (this.login.role == "ADMIN") {
        let path = "admin";
        this.router.navigate([path]);
      }
      if (this.login.role == "EMPLOYEE") {
        let path = "emp";
        this.netService.data = this.login;
        this.router.navigate([path]);
      }
      this.status = false;
    } else {
      this.status = true;
    }
  }
}
