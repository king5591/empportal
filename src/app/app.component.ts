import { NetService } from "./net.service";
import { Component } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  data;
  url: string = "http://localhost:2410/empapp/emps";

  constructor(private netService: NetService) {}

  ngOnInit() {
    this.netService.getData(this.url).subscribe((resp) => {
      this.data = resp;
      // console.log(this.data);
    });
  }
}
