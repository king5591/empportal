import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl,
  ValidatorFn,
} from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { NetService } from "./../net.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-emp",
  templateUrl: "./emp.component.html",
  styleUrls: ["./emp.component.css"],
})
export class EmpComponent implements OnInit {
  contactForm: FormGroup = null;
  billForm: FormGroup = null;
  hotelForm: FormGroup = null;
  travelForm: FormGroup = null;
  emps = ["Contact Details", "Bills"];
  data;
  contactUrl = "http://localhost:2410/empapp/empcontact/";
  billUrl = "http://localhost:2410/empapp/empbills/2/";
  url = "http://localhost:2410/empapp/";
  empport: string = null;
  contact;
  bill;
  contactStat = 0;
  conatactMsg = "";
  billstat = 0;
  billMsg = "";
  billshow = false;
  billid;
  day = [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "11",
    "12",
    "13",
    "14",
    "15",
    "16",
    "17",
    "18",
    "19",
    "20",
    "21",
    "22",
    "23",
    "24",
    "25",
    "26",
    "27",
    "28",
    "29",
    "30",
    "31",
  ];
  month = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  years = ["2018", "2019", "2020"];
  hotelstatus = 0;
  hotelMsg = "";
  travelstatus = 0;
  travelMsg = "";

  constructor(
    private netService: NetService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.data = this.netService.data;
    // console.log(this.data);
    this.route.paramMap.subscribe((param) => {
      this.empport = param.get("portal");
      this.billid = param.get("id");
      // console.log(this.billid);
      if (this.empport == "contact") {
        this.hotelstatus = 0;
        this.billshow = false;
        this.billstat = 0;
        this.netService.getData(this.contactUrl + 2).subscribe(
          (resp) => {
            this.contact = resp;
            // console.log(this.contact);
            this.createContactForm();

            this.contactForm.get("mobile").setValue(this.contact.mobile);
            this.contactForm.get("address").setValue(this.contact.address);
            this.contactForm.get("city").setValue(this.contact.city);
            this.contactForm.get("country").setValue(this.contact.country);
            this.contactForm.get("pin").setValue(this.contact.pincode);
          },
          (error) => {
            console.log(error);
          }
        );
      }
      if (this.empport == "bills") {
        this.hotelstatus = 0;
        this.createBillsForm();
        this.netService.getData(this.billUrl).subscribe((resp) => {
          this.bill = resp;
          // console.log(this.bill);
        });
      }
      if (this.empport == "hotelbill") {
        this.netService
          .getData(this.url + this.empport + "/2/" + this.billid)
          .subscribe(
            (resp) => {
              this.createHotelForm();
              // this.createTravelForm();
              this.data = resp;
              console.log(this.data);
              let temp1 = this.data.staystartdate.split("-");
              let temp2 = this.data.stayenddate.split("-");
              this.hotelForm.get("ciday").setValue(temp1[0]);
              this.hotelForm.get("cimonth").setValue(temp1[1]);
              this.hotelForm.get("ciyear").setValue(temp1[2]);
              this.hotelForm.get("coday").setValue(temp2[0]);
              this.hotelForm.get("comonth").setValue(temp2[1]);
              this.hotelForm.get("coyear").setValue(temp2[2]);
              this.hotelForm.get("hotel").setValue(this.data.hotel);
              this.hotelForm.get("city").setValue(this.data.city);
              this.hotelForm.get("booking").setValue(this.data.corpbooking);
            },
            (error) => {
              console.log(error);
            }
          );
      }
      if (this.empport == "travelbill") {
        this.netService
          .getData(this.url + this.empport + "/2/" + this.billid)
          .subscribe(
            (resp) => {
              this.data = resp;
              console.log(this.data);
              this.createTravelForm();
              let temp1 = this.data.goflightDate.split("-");
              let temp2 = this.data.backflightDate.split("-");
              this.travelForm.get("dday").setValue(temp1[0]);
              this.travelForm.get("dmonth").setValue(temp1[1]);
              this.travelForm.get("dyear").setValue(temp1[2]);
              this.travelForm.get("rday").setValue(temp2[0]);
              this.travelForm.get("rmonth").setValue(temp2[1]);
              this.travelForm.get("ryear").setValue(temp2[2]);
              this.travelForm
                .get("originCity")
                .setValue(this.data.goflightOrigin);
              this.travelForm.get("destCity").setValue(this.data.goflightDest);
              this.travelForm.get("flightNo").setValue(this.data.goflightNum);
              this.travelForm
                .get("roriginCity")
                .setValue(this.data.backflightOrigin);
              this.travelForm
                .get("rdestCity")
                .setValue(this.data.backflightDest);
              this.travelForm
                .get("rflightNo")
                .setValue(this.data.backflightNum);
              this.travelForm.get("booking").setValue(this.data.corpbooking);
            },
            (error) => {
              console.log(error);
            }
          );
      }
    });
  }
  createTravelForm() {
    this.travelForm = new FormGroup({
      dday: new FormControl("", [Validators.required]),
      dmonth: new FormControl("", [Validators.required]),
      dyear: new FormControl("", [Validators.required]),
      rday: new FormControl("", [Validators.required]),
      rmonth: new FormControl("", [Validators.required]),
      ryear: new FormControl("", [Validators.required]),
      originCity: new FormControl("", [Validators.required]),
      destCity: new FormControl("", [Validators.required]),
      flightNo: new FormControl("", [Validators.required]),
      roriginCity: new FormControl("", [Validators.required]),
      rdestCity: new FormControl("", [Validators.required]),
      rflightNo: new FormControl("", [Validators.required]),
      booking: new FormControl(""),
    });
  }
  get dday() {
    return this.travelForm.get("dday");
  }
  get dmonth() {
    return this.travelForm.get("dmonth");
  }
  get dyear() {
    return this.travelForm.get("dyear");
  }
  get rday() {
    return this.travelForm.get("rday");
  }
  get rmonth() {
    return this.travelForm.get("rmonth");
  }
  get ryear() {
    return this.travelForm.get("ryear");
  }
  get originCity() {
    return this.travelForm.get("originCity");
  }
  get destCity() {
    return this.travelForm.get("destCity");
  }
  get flightNo() {
    return this.travelForm.get("flightNo");
  }
  get roriginCity() {
    return this.travelForm.get("roriginCity");
  }
  get rdestCity() {
    return this.travelForm.get("rdestCity");
  }
  get rflightNo() {
    return this.travelForm.get("rflightNo");
  }
  get booking() {
    return this.travelForm.get("booking");
  }

  createHotelForm() {
    this.hotelForm = new FormGroup({
      ciday: new FormControl("", [Validators.required]),
      cimonth: new FormControl("", [Validators.required]),
      ciyear: new FormControl("", [Validators.required]),
      coday: new FormControl("", [Validators.required]),
      comonth: new FormControl("", [Validators.required]),
      coyear: new FormControl("", [Validators.required]),
      hotel: new FormControl("", [Validators.required]),
      city: new FormControl("", [Validators.required]),
      booking: new FormControl(""),
    });
  }
  get ciday() {
    return this.hotelForm.get("ciday");
  }
  get cimonth() {
    return this.hotelForm.get("cimonth");
  }
  get ciyear() {
    return this.hotelForm.get("ciyear");
  }
  get coday() {
    return this.hotelForm.get("coday");
  }
  get comonth() {
    return this.hotelForm.get("comonth");
  }
  get coyear() {
    return this.hotelForm.get("coyear");
  }
  get hotel() {
    return this.hotelForm.get("hotel");
  }
  get city() {
    return this.hotelForm.get("city");
  }
  get bank() {
    return this.hotelForm.get("booking");
  }

  newBill() {
    this.billshow = true;
  }
  createBillsForm() {
    this.billForm = new FormGroup({
      des: new FormControl("", [Validators.required]),
      expType: new FormControl("", [Validators.required]),
      amount: new FormControl("", [Validators.required, this.price()]),
    });
  }
  price(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      let p1 = control.value;
      let ok = false;
      if (p1.match(/[0-9]/) && !p1.match(/[a-z]/i)) {
        ok = true;
      } else {
        ok = false;
      }
      return ok ? null : { price: true };
    };
  }
  get des() {
    return this.billForm.get("des");
  }
  get expType() {
    return this.billForm.get("expType");
  }
  get amount() {
    return this.billForm.get("amount");
  }
  onBillSubmit() {
    let id = this.data.empuserid;
    let des1 = this.billForm.get("des").value;
    let exp = this.billForm.get("expType").value;
    let amt = this.billForm.get("amount").value;
    // console.log(exp);
    // console.log(amt);
    let obj = {
      empuserid: id,
      description: des1,
      expensetype: exp,
      amount: amt,
    };
    this.netService.postData(this.billUrl, obj).subscribe(
      (resp) => {
        // console.log("Data successfully uploaded");
        this.billstat = 1;
        this.billMsg = "New Bill has been successfully created";
      },
      (error) => {
        this.billstat = 2;
        this.billMsg = error.message;
      }
    );
    this.netService.getData(this.billUrl).subscribe((resp) => {
      this.bill = resp;
      // console.log(this.bill);
    });
  }

  createContactForm() {
    this.contactForm = new FormGroup({
      mobile: new FormControl("", [
        Validators.required,
        Validators.minLength(10),
        this.matchNo(),
      ]),
      address: new FormControl(""),
      city: new FormControl(""),
      country: new FormControl("", [Validators.required]),
      pin: new FormControl("", [Validators.required]),
    });
  }
  matchNo(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      let m1 = control.value;
      let ok = false;
      if (m1.match(/[0-9]/) && m1.match(/[+,-,' ']/)) {
        ok = true;
      } else {
        ok = false;
      }
      return ok ? null : { matchNo: true };
    };
  }
  get mobile() {
    return this.contactForm.get("mobile");
  }
  get country() {
    return this.contactForm.get("country");
  }
  get pin() {
    return this.contactForm.get("pin");
  }
  onSubmit() {
    this.createContactForm();
    let id = this.data.empuserid;
    this.contactUrl += 3;
    let mob = this.contactForm.get("mobile").value;
    let addr = this.contactForm.get("address").value;
    let city1 = this.contactForm.get("city").value;
    let ctry = this.contactForm.get("country").value;
    let pin1 = this.contactForm.get("pin").value;

    let obj = {
      empuserid: id,
      mobile: mob,
      address: addr,
      city: city1,
      country: ctry,
      pincode: pin1,
    };
    this.netService.postData(this.contactUrl, obj).subscribe(
      (resp) => {
        this.contactStat = 1;
        this.conatactMsg = "Details has been successfully added";
      },
      (error) => {
        this.contactStat = 2;
        this.conatactMsg = error.message;
      }
    );
  }

  details() {
    this.route.paramMap.subscribe((param) => {
      this.billid = param.get("portal");
    });
  }
  onHotelbillSubmit() {
    let day1 = this.hotelForm.get("ciday").value;
    let month1 = this.hotelForm.get("cimonth").value;
    let year1 = this.hotelForm.get("ciyear").value;
    let day2 = this.hotelForm.get("coday").value;
    let month2 = this.hotelForm.get("comonth").value;
    let year2 = this.hotelForm.get("coyear").value;

    let startdate = day1 + "-" + month1 + "-" + year1;
    let enddate = day2 + "-" + month2 + "-" + year2;
    let hotel1 = this.hotelForm.get("hotel").value;
    let city1 = this.hotelForm.get("city").value;
    let booking1 = this.hotelForm.get("booking").value;
    let book1;
    if (booking1) {
      book1 = "Yes";
    } else {
      book1 = "No";
    }
    let obj = {
      amount: this.data.amount,
      billid: this.billid,
      city: city1,
      corpbooking: book1,
      description: this.data.description,
      empuserid: this.data.empuserid,
      expensetype: this.data.expensetype,
      hotel: hotel1,
      stayenddate: enddate,
      staystartdate: startdate,
    };
    console.log(obj);
    // console.log(this.url + this.empport);
    this.netService.postData(this.url + this.empport, obj).subscribe(
      (resp) => {
        this.hotelstatus = 1;
        this.hotelMsg = "Hotel Stay Details has been successfully created";
      },
      (error) => {
        this.hotelstatus = 2;
        this.hotelMsg = error.messsage;
      }
    );
  }
  onTravelbillSubmit() {
    let day1 = this.travelForm.get("dday").value;
    let month1 = this.travelForm.get("dmonth").value;
    let year1 = this.travelForm.get("dyear").value;
    let day2 = this.travelForm.get("rday").value;
    let month2 = this.travelForm.get("rmonth").value;
    let year2 = this.travelForm.get("ryear").value;
    let date1 = day1 + "-" + month1 + "-" + year1;
    let date2 = day2 + "-" + month2 + "-" + year2;

    let origin1 = this.travelForm.get("originCity").value;
    let city1 = this.travelForm.get("destCity").value;
    let flight1 = this.travelForm.get("flightNo").value;
    let origin2 = this.travelForm.get("roriginCity").value;
    let city2 = this.travelForm.get("rdestCity").value;
    let flight2 = this.travelForm.get("rflightNo").value;
    let booking1 = this.travelForm.get("booking").value;
    let book1;
    if (booking1) {
      book1 = "Yes";
    } else {
      book1 = "No";
    }
    let obj = {
      amount: this.data.amount,
      backflightDate: date2,
      backflightDest: city2,
      backflightNum: flight2,
      backflightOrigin: origin2,
      billid: this.data.billid,
      corpbooking: book1,
      description: this.data.description,
      empuserid: this.data.empuserid,
      expensetype: this.data.expensetype,
      goflightDate: date1,
      goflightDest: city2,
      goflightNum: flight1,
      goflightOrigin: origin1,
    };
    // console.log(obj);
    // console.log(this.url + this.empport);
    this.netService.postData(this.url + this.empport, obj).subscribe(
      (resp) => {
        this.travelstatus = 1;
        this.travelMsg = "Flight Details has been successfully created";
      },
      (error) => {
        this.travelstatus = 2;
        this.travelMsg = error.messsage;
      }
    );
  }
}
