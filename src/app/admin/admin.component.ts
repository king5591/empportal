import {
  FormGroup,
  FormControl,
  Validators,
  ValidatorFn,
  AbstractControl,
} from "@angular/forms";
import { NetService } from "./../net.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.css"],
})
export class AdminComponent implements OnInit {
  simpleForm: FormGroup = null;
  empDetails: string = null;
  url: string = "http://localhost:2410/empapp/emps";
  depUrl = "http://localhost:2410/empapp/empdept/";
  data;
  admin = [];
  home = true;
  canSubmit = true;
  afterStatus = 0;
  showAddMsg = "";
  detail = false;
  deptForm: FormGroup = null;
  depstat = 0;
  depmsg = "";
  add = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private netService: NetService
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe((param) => {
      this.empDetails = param.get("empDetail");
      // console.log(this.empDetails);
    });
    this.netService.getData(this.url).subscribe((resp) => {
      this.data = resp;
      this.data = this.data.data;
      this.admin = this.data.find((f1) => f1.role == "ADMIN");
      // console.log(this.admin);
    });
    this.createForm();
  }

  createForm() {
    this.simpleForm = new FormGroup(
      {
        name: new FormControl("", [
          Validators.required,
          Validators.minLength(8),
        ]),
        email: new FormControl("", [Validators.required, Validators.email]),
        password: new FormControl("", [
          Validators.required,
          Validators.minLength(8),
          this.patternMatch(),
        ]),
        repass: new FormControl(""),
      },
      { validators: this.passMatch, updateOn: "submit" }
    );
  }

  patternMatch(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      var p1 = control.value;
      var ok = false;
      if (p1.match(/[a-z]/) && p1.match(/[A-Z]/) && p1.match(/[0-9]/)) {
        ok = true;
      } else {
        ok = false;
      }
      return ok ? null : { patternMatch: true };
    };
  }

  passMatch(fg: FormGroup) {
    var p1 = fg.get("password").value;
    var p2 = fg.get("repass").value;
    var ok = p1 != p2;
    if (p1.length == 0 && p2.length == 0) {
      ok = true;
    }
    return ok ? null : { passMatch: true };
  }

  get name() {
    return this.simpleForm.get("name");
  }
  get email() {
    return this.simpleForm.get("email");
  }
  get password() {
    return this.simpleForm.get("password");
  }
  get repass() {
    return this.simpleForm.get("repass");
  }

  onSubmit() {
    let name1 = this.simpleForm.get("name").value;
    let email1 = this.simpleForm.get("email").value;
    let pass = this.simpleForm.get("password").value;
    let obj = { name: name1, email: email1, password: pass, role: "Employee" };
    this.canSubmit = false;
    this.netService.postData(this.url, obj).subscribe(
      (data) => {
        this.processData(data);
      },
      (error) => {
        this.afterStatus = 2;
        this.showAddMsg = error.message;
      }
    );
  }
  processData(data) {
    this.afterStatus = 1;
    this.showAddMsg = "Employee has been successfully added";
  }

  details(i) {
    this.detail = true;
    this.empDetails = null;
    this.depUrl += i;
    this.deptForm = new FormGroup({
      dep: new FormControl("", {
        validators: [Validators.required],
        updateOn: "submit",
      }),
      deg: new FormControl("", {
        validators: [Validators.required],
        updateOn: "submit",
      }),
      manager: new FormControl("", {
        validators: [Validators.required],
        updateOn: "submit",
      }),
    });
    let respdata;
    this.netService.getData(this.depUrl).subscribe(
      (resp) => {
        respdata = resp;
        // console.log(resp);
        this.deptForm.get("dep").setValue(respdata.department);
        this.deptForm.get("deg").setValue(respdata.designation);
        this.deptForm.get("manager").setValue(respdata.manager);
      },
      (error) => {
        console.log(error);
      }
    );
  }
  depSubmit() {
    this.add = true;
    console.log(this.detail);
    let id = 7;
    let dept = this.deptForm.get("dep").value;
    let desig = this.deptForm.get("deg").value;
    let mgr = this.deptForm.get("manager").value;
    let obj = {
      empuserid: id,
      designation: desig,
      department: dept,
      manager: mgr,
    };
    this.netService.postData(this.depUrl, obj).subscribe(
      (param) => {
        console.log(param);
        this.depstat = 1;
        this.depmsg = "Details has been successfully added";
        console.log(this.depmsg);
      },
      (error) => {
        this.depstat = 2;
        this.depmsg = error.message;
      }
    );
  }
}
